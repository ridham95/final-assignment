﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplicationFinalProject
{
    public partial class DefaultPage : System.Web.UI.Page
    {
        private string main_query = "SELECT Pageid,Pagetitle,Pagecontent from myPage";
        protected void Page_Load(object sender, EventArgs e)
        {
            select_pages.SelectCommand = main_query;
            list_pages.DataSource = Classes_Manual_Bind(select_pages);
            list_pages.DataBind();
        }
        protected DataView Classes_Manual_Bind(SqlDataSource src)
        {

            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {

                row["Pagetitle"] =
                    "<a href=\"default1.aspx?pageid="
                    + row["Pageid"]
                    + "\">"
                    + row["Pagetitle"]
                    + "</a>";
            }

            mytbl.Columns.Remove("Pageid");
            myview = mytbl.DefaultView;

            return myview;

        }
    }
}