﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplicationFinalProject
{
    public partial class PageEdit : System.Web.UI.Page
    {
        public int Pageid
        {
            get { return Convert.ToInt32(Request.QueryString["Pageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void add_content(object sender, EventArgs e)
        {

            string title = titlepage1.Text;
            string content = contentpage1.Text;
            string editquery = "Update myPage set Pagetitle='" + title + "'," +
                  "Pagecontent= '" + content + "'" +
                  " where Pageid=" + Pageid;


            select_pages.UpdateCommand = editquery;
            select_pages.Update();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            DataRowView studentrow = getClassInfo(Pageid);
            if (studentrow == null)
            {
                page_edit.InnerHtml = "No Pages Found.";
                return;
            }
            titlepage1.Text = studentrow["Pagetitle"].ToString();
            contentpage1.Text = studentrow["Pagecontent"].ToString();

        }
        protected DataRowView getClassInfo(int id)
        {
            string query = "select * from myPage where Pageid=" + Pageid.ToString();
            select_pages.SelectCommand = query;

            DataView classview = (DataView)select_pages.Select(DataSourceSelectArguments.Empty);


            if (classview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView classrowview = classview[0];
            return classrowview;
        }
    }
}