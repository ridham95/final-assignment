﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DefaultPage.aspx.cs" Inherits="WebApplicationFinalProject.DefaultPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <h2>Manage My Pages</h2>
    <asp:SqlDataSource runat="server"
        id="select_pages"
       
        ConnectionString="<%$ ConnectionStrings:ridham_database %>">
    </asp:SqlDataSource>
    
    <asp:DataGrid id="list_pages"
        runat="server" >
    </asp:DataGrid>
    <a href="Add.aspx">New Page..</a>
</asp:Content>
