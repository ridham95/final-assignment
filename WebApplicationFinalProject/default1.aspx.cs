﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplicationFinalProject
{
    public partial class default1 : System.Web.UI.Page
    {
        public string Pageid
        {
            get { return Request.QueryString["Pageid"]; }
        }
        private string query = "SELECT Pageid,Pagetitle,Pagecontent from myPage";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Pageid == "" || Pageid == null) title.InnerHtml = "pages not found.";
            else
            {

                query += " WHERE PAGEID = " + Pageid;

                select_pages.SelectCommand = query;

                DataView classview = (DataView)select_pages.Select(DataSourceSelectArguments.Empty);
                string page_title = classview[0]["Pagetitle"].ToString();
                title.InnerHtml = page_title;
                string page_content = classview[0]["Pagecontent"].ToString();
                fetch1.InnerHtml = page_content;
            }
        }
        protected void Del(object sender, EventArgs e)
        {

            string del_query = "DELETE FROM myPage WHERE Pageid=" + Pageid;

            del_debug.InnerHtml = del_query;
            del.DeleteCommand = del_query;
            del.Delete();


            string del_query1 = "DELETE FROM" +
                " myPage WHERE PAGEID=" + Pageid;
            del_debug.InnerHtml += "<br>BUT ALSO<br>" + del_query1;
            del.DeleteCommand = del_query1;
            del.Delete();
        }
    }
}