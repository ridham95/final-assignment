﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default1.aspx.cs" Inherits="WebApplicationFinalProject.default1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3 id="title" runat="server"></h3>
    
     <asp:SqlDataSource runat="server"
        id="select_pages"
       
        ConnectionString="<%$ ConnectionStrings:ridham_database %>">
    </asp:SqlDataSource>
    <div id="fetch1" runat="server">

    </div>
    <asp:SqlDataSource 
        runat="server"
        id="del"
        ConnectionString="<%$ ConnectionStrings:ridham_database %>">
    </asp:SqlDataSource>
    <asp:Button runat="server" id="del_page_btn"
        OnClick="Del"
        OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete" />
    <div id="del_debug" class="querybox" runat="server"></div>
    <a href="PageEdit.aspx?Pageid=<%Response.Write(this.Pageid);%>">Update! Update!</a>
</asp:Content>
