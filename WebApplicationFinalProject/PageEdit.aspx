﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PageEdit.aspx.cs" Inherits="WebApplicationFinalProject.PageEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3 runat="server" id="page_edit"> Edit</h3>
    <div>
       <asp:Label ID="titlepage" runat="server" Text="Title Page"></asp:Label>
        <asp:TextBox ID="titlepage1" runat="server"></asp:TextBox>
    </div>
    <asp:SqlDataSource runat="server"
        id="select_pages"
       
        ConnectionString="<%$ ConnectionStrings:ridham_database %>">
    </asp:SqlDataSource>
    <div>
       <asp:Label ID="contentpage" runat="server" Text="Content of the Page"></asp:Label>
        <asp:TextBox ID="contentpage1" runat="server"></asp:TextBox>
    </div>
    <div><asp:Button ID="Btn" runat="server" Text="Submit" OnClick="add_content" /></div>
</asp:Content>
