﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplicationFinalProject.usercontrols
{
    public partial class mymenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = "select * from myPage";
            //inclass example help
            DataView view1 = (DataView)pages_select.Select(DataSourceSelectArguments.Empty);

            string content_menu = "";
            foreach(DataRowView myrow in view1)
            {
                content_menu += "<a href=\"default1.aspx?Pageid="
                    + myrow["Pageid"]
                    + "\">"
                    + myrow["Pagetitle"]
                    + "</a>";

            }
            menu_bar.InnerHtml = content_menu;
        }
    }
}